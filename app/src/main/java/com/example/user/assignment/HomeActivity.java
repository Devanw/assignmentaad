package com.example.user.assignment;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HomeActivity extends AppCompatActivity{

    private static final String TAG = "HomeActivity";
    private ListView listView;
    private String[] recipeNames;
    private String[] recipeDetails;
    private String[] ingredientName;
    private String[] direction;
    public ArrayList<Recipe> recipes = new ArrayList<>();
    public TypedArray recipepic;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("CookBook");

    public HomeActivity(){};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Log.i(TAG, "onCreate: ");



        listView = (ListView) findViewById(R.id.listViewComplex);
        listView.setOnItemClickListener(

                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent (view.getContext(), RecipeDetails.class);
                        intent.putExtra("recipe_extra_id", recipes.get(position).getID());
                        intent.putExtra("recipe_extra_name", recipes.get(position).getName());
                        intent.putExtra("recipe_extra_jpg", recipes.get(position).getPhoto());
                        intent.putExtra("recipe_extra_ing", recipes.get(position).getIngredients());
                        intent.putExtra("recipe_extra_dir", recipes.get(position).getDirection());
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        Toast.makeText(getBaseContext(), "You clicked " + recipes.get(position).getDetail(), Toast.LENGTH_SHORT).show();
                    }
                }
        );

    }

    @Override
    protected void onStart() {
        super.onStart();
        recipeNames = getResources().getStringArray(R.array.recipeNames);
        recipeDetails = getResources().getStringArray(R.array.recipeDetails);
        ingredientName = getResources().getStringArray(R.array.ingredientNames);
        direction = getResources().getStringArray(R.array.direction);
        recipepic = getResources().obtainTypedArray(R.array.recipepic);

        myRef.child("Recipe1").setValue(new Recipe("Recipe1", recipeNames[0], recipeDetails[0], recipepic.getResourceId(0, 0), ingredientName[0], direction[0]));
        myRef.child("Recipe2").setValue(new Recipe("Recipe2", recipeNames[1], recipeDetails[1], recipepic.getResourceId(1, 0), ingredientName[1], direction[1]));
        myRef.child("Recipe3").setValue(new Recipe("Recipe3", recipeNames[2], recipeDetails[2], recipepic.getResourceId(2, 0), ingredientName[2], direction[2]));

        generateRecipes();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                recipes.clear();
                for (DataSnapshot recipeSnapshot : dataSnapshot.getChildren()) {
                    Recipe recipee = recipeSnapshot.getValue(Recipe.class);
                    recipes.add(recipee);
                }
                listView.setAdapter(new RecipeAdapter(HomeActivity.this, R.layout.list_item, recipes));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(HomeActivity.this, "Database Error", Toast.LENGTH_SHORT).show();
            }
        });

        recipepic.recycle();
    }


    private void generateRecipes() {

        for (int i = 0; i < direction.length; i++) {
            recipes.add(new Recipe("Recipe"+(i+1),recipeNames[i], recipeDetails[i], recipepic.getResourceId(i,0), ingredientName[i], direction[i]));
      }

    }

    public void newRecipeIntent (View view){
        Intent i = new Intent (view.getContext(), addNewRecipe.class );
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    };

    public void onBackPressed() {
        Intent i = new Intent (this, MainActivity.class );
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
