package com.example.user.assignment;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class RecipeDetails extends AppCompatActivity {

    private String extraname;
    private String extraingredients;
    private int extraimg;
    private String extradirection;
    private String extraid;
    private static final String TAG = "RecipeDetails";
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("CookBook");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_details);
        extraname = getIntent().getExtras().getString("recipe_extra_name");
        extraingredients = getIntent().getExtras().getString("recipe_extra_ing");
        extraimg = getIntent().getExtras().getInt("recipe_extra_jpg");
        extradirection = getIntent().getExtras().getString("recipe_extra_dir");
        extraid = getIntent().getExtras().getString("recipe_extra_id");

        final TextView textView = (TextView) findViewById(R.id.textView);
        final TextView textView2 = (TextView) findViewById(R.id.textView2);
        final ImageView imageView = (ImageView) findViewById(R.id.imageViewdet);
        final TextView textView3 = (TextView) findViewById(R.id.textView3);
        final TextView textView4 = (TextView) findViewById(R.id.textView4);

        textView.setText(extraname);
        textView2.setText(extraingredients);
        imageView.setImageResource(extraimg);
        textView3.setText(extradirection);
        textView4.setText("Ingredients");

        Button togglevisibility = (Button) findViewById(R.id.visibilityswitch);
        togglevisibility.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(textView3.getVisibility()==View.INVISIBLE){
                            textView3.setVisibility(View.VISIBLE);
                            textView2.setVisibility(View.INVISIBLE);
                            textView4.setText("Direction");}
                        else if(textView3.getVisibility()==View.VISIBLE){
                            textView3.setVisibility(View.INVISIBLE);
                            textView2.setVisibility(View.VISIBLE);
                            textView4.setText("Ingredients");}
                    }}
        );

        Button deletebutton = (Button) findViewById(R.id.button4);
        deletebutton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(TAG, "onClick: remove");
                        //database.getReference("CookBook");
                        Log.d(TAG, "extraid : " + extraid);
                        myRef.child(extraid).removeValue();
                        Log.i(TAG, "onClick: doneremove");
                        Intent i = new Intent(v.getContext(), HomeActivity.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    }}
        );
    }

    public void onBackPressed() {
        Intent i = new Intent (this, HomeActivity.class );
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }





}
