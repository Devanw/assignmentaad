package com.example.user.assignment;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;

public class Recipe {
    private String id;
    private String name;
    private String detail;
    private int photo;
    private String ingredients;
    private String direction;

    public Recipe(){};

    public Recipe(String id, String name, String detail, int photo, String ingredients, String direction) {
        this.id = id;
        this.name = name;
        this.detail = detail;
        this.photo = photo;
        this.ingredients = ingredients;
        this.direction = direction;
    }
    public Recipe(String id, String name, String detail, String ingredients, String direction) {
        this.id = id;
        this.name = name;
        this.detail = detail;
        this.photo = R.drawable.food1;
        this.ingredients = ingredients;
        this.direction = direction;
    }

    public String getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDetail() {
        return detail;
    }

    public int getPhoto() {
        return photo;
    }

    public String getIngredients() {return ingredients;}

    public String getDirection() {return direction;}

    @Override
    public String toString() {
        return detail;
    }
}
