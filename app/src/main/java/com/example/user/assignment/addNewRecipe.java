package com.example.user.assignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class addNewRecipe extends AppCompatActivity {

    private EditText editTextName;
    private EditText editTextDesc;
    private EditText editTextIng;
    private EditText editTextDir;

    public HomeActivity home= new HomeActivity();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("CookBook");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_recipe);
    }

    public void saveButtonClick(View view){
        editTextName = (EditText) findViewById(R.id.inName);
        editTextDesc = (EditText) findViewById(R.id.inDesc);
        editTextIng = (EditText) findViewById(R.id.inIng);
        editTextDir = (EditText) findViewById(R.id.inDir);

        String name = editTextName.getText().toString();
        String desc = editTextDesc.getText().toString();
        String ing = editTextIng.getText().toString();
        String dir = editTextDir.getText().toString();

        String keyforrecipe = myRef.push().getKey();
        myRef.child(keyforrecipe).setValue(new Recipe(keyforrecipe,name,desc,ing,dir));

        startActivity(new Intent (view.getContext(),HomeActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void onBackPressed() {
        Intent i = new Intent (this, HomeActivity.class );
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
